package com.gkemayo.lambda;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;

import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.utils.CollectionUtils;
import software.amazon.awssdk.utils.StringUtils;

public class DemoAwsLambdaHandler implements RequestHandler<SQSEvent, String> {

	private String awsBucketName = "prod-demo-aws-lambda-s3";

	private String prefixKeyName = "demo-aws-lambda-file-";

	// Paris
	private Region region = Region.EU_WEST_3;

	@Override
	public String handleRequest(SQSEvent input, Context context) {

		String result = "OK";

		LambdaLogger logger = context.getLogger();

		try {
			S3Client s3Client = S3Client.builder().region(region).build();

			long currentTime = System.currentTimeMillis();
			String awsS3FileKeyName = prefixKeyName + currentTime;

			String fileName = "data-" + currentTime;
			File myFile = new File(System.getProperty("java.io.tmpdir") + File.separator + fileName.concat(".txt"));

			if (!CollectionUtils.isNullOrEmpty(input.getRecords())) {
				try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(myFile, true))) {
					
					input.getRecords().forEach(message -> {
						try {
							if (StringUtils.isNotBlank(message.getBody())) {
								bufferedWriter.append(message.getBody());
								bufferedWriter.newLine();
							}
						} catch (IOException e) {
							throw new RuntimeException("An error occurs when writing data in file " + fileName + " : " + e);
						}
					});
					
				} catch (IOException e) {
					throw new RuntimeException("An error occurs in BufferedWriter : " + e);
				}
			}

			s3Client.putObject(PutObjectRequest.builder().bucket(awsBucketName).key(awsS3FileKeyName).build(), RequestBody.fromFile(myFile));
			
			myFile.delete();
		} catch (RuntimeException e) {
			logger.log(e.getMessage());
			result = "KO";
		}
		
		// s3Client.getObject(GetObjectRequest.builder().bucket(awsBucketName).key(awsS3FileKeyName).build(), myFile.toPath());

		return result;
	}

}
